job "jobs-registry" {
    datacenters = ["dc1"]
    type = "service"
    group "group-registry" {
        count = 1
        network {
            port "http" {
                to = 6161
            }
        }
        service{
            name = "serviceregistry"
            tags = ["tag-registry" ,"tag_2"]
            port = "http"
        }
        task "task-registry" {
            driver = "docker"
            config {
                image = "registry.gitlab.com/legeyvaz/registry:latest"
                ports = ["http"]
            }
            resources {
                cpu = 600
                memory = 128
            }
        }
    }
}
